/**
 * Created by Dmitry on 6/17/2015.
 */
import java.text.DecimalFormat;

public class ProductionWorker extends Employee{
    private int shift;
    private double payRate;
    public static final int DAY_SHIFT = 1;
    public static final int NIGHT_SHIFT = 2;

    public ProductionWorker(String n, String num, String date, int sh, double rate){
        super(n, num, date);
        shift = sh;
        payRate = rate;
    }

    public ProductionWorker(){
        super();
        shift = DAY_SHIFT;
        payRate = 0.0;
    }

    public void setShift(int sh) {
        shift = sh;
    }

    public void setPayRate(double rate) {
        payRate = rate;
    }

    public int getShift() {
        return shift;
    }

    public double getPayRate() {
        return payRate;
    }

    public String toString() {
        DecimalFormat dollar = new DecimalFormat("#,##0.00");

        String str = super.toString();

        str += "\nShift: ";
        if (shift == DAY_SHIFT)
            str += "Day";
        else if (shift == NIGHT_SHIFT)
            str += "Night";
        else
            str += "Invalid Shift Number";

        str += ("\nHourly Pay Rate: $" + dollar.format(payRate));
        return str;
    }
}
